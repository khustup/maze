#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct
{
    unsigned char m_walls;
} Cell;

typedef struct
{
    Cell** m_cells;
    int m_width;
    int m_height;
    int m_start_x;
    int m_start_y;
    int m_end_x;
    int m_end_y;
} Maze;

int is_up_wall(Cell* c);
int is_left_wall(Cell* c);
int is_bottom_wall(Cell* c);
int is_right_wall(Cell* c);
void make_up_wall(Cell* c);
void make_left_wall(Cell* c);
void make_bottom_wall(Cell* c);
void make_right_wall(Cell* c);

Maze* read_maze(const char* input);
void display_maze(const Maze* maze);
char* find_path(const Maze* maze);
void output_path(const char* output, const char* path);

int main(int argc, char** argv)
{
    if (argc < 3) {
        puts ("Invalid arguments count.");
        return 1;
    }
    const char* input = NULL;
    const char* output = NULL;
    int display = 0;
    if (argc == 3 || strcmp(argv[1], "-d") != 0) {
        input = argv[1];
        output = argv[2];
    } else {
        display = 1;
        input = argv[2];
        output = argv[3];
    }
    Maze* maze = read_maze(input);
    if (maze == NULL) {
        return 1;
    }
    if (display) {
        display_maze(maze);
    }
    char* path = find_path(maze);
    if (path == NULL) {
        puts ("There is no path between specified positions.");
    } else {
        output_path(output, path);
    }
    return 0;
}

int is_up_wall(Cell* c)
{
    return (c->m_walls & 8) != 0;
}

int is_left_wall(Cell* c)
{
    return (c->m_walls & 4) != 0;
}

int is_bottom_wall(Cell* c)
{
    return (c->m_walls & 2) != 0;
}

int is_right_wall(Cell* c)
{
    return (c->m_walls & 1) != 0;
}

void make_up_wall(Cell* c)
{
    c->m_walls |= 8;
}

void make_left_wall(Cell* c)
{
    c->m_walls |= 4;
}

void make_bottom_wall(Cell* c)
{
    c->m_walls |= 2;
}

void make_right_wall(Cell* c)
{
    c->m_walls |= 1;
}

int check_maze(const Maze* maze)
{
    if (maze->m_start_x < 0 || maze->m_start_x > maze->m_width || maze->m_start_y < 0 || maze->m_start_y > maze->m_height) {
        puts ("ERROR: Start point is outside of maze.");
        return 0;
    }
    if (maze->m_end_x < 0 || maze->m_end_x > maze->m_width || maze->m_end_y < 0 || maze->m_end_y > maze->m_height) {
        puts ("ERROR: Start point is outside of maze.");
        return 0;
    }
    for (int i = 0; i < maze->m_height - 1; ++i) {
        for (int j = 0; j < maze->m_width - 1; ++j) {
            if (is_right_wall(&maze->m_cells[i][j]) != is_left_wall(&maze->m_cells[i][j + 1])) {
                printf ("ERROR: Wrong cells (%d, %d) and (%d, %d)\n", i, j, i, j + 1);
                return 0;
            }
            if (is_bottom_wall(&maze->m_cells[i][j]) != is_up_wall(&maze->m_cells[i + 1][j])) {
                printf ("ERROR: Wrong cells (%d, %d) and (%d, %d)\n", i, j, i + 1, j);
                return 0;
            }
        }
    }
    return 1;
}

Maze* read_maze(const char* input)
{
    FILE* f = fopen(input, "r");
    if (f == NULL) {
        printf ("ERROR: Failed to open file %s for reading.\n", input);
        return NULL;
    }
    Maze* maze = (Maze*) malloc(sizeof(Maze));
    int count = 0;
    count = fscanf(f, "%d %d %d %d %d %d", 
                   &maze->m_height,
                   &maze->m_width,
                   &maze->m_start_y,
                   &maze->m_start_x,
                   &maze->m_end_y,
                   &maze->m_end_x);
    if (count != 6) {
        printf ("ERROR: File %s has invalid format.\n", input);
        free(maze);
        fclose(f);
        return NULL;
    }
    maze->m_cells = (Cell**) malloc(sizeof(Cell*) * maze->m_height);
    for (int i = 0; i < maze->m_height; ++i) {
        maze->m_cells[i] = (Cell*) malloc(sizeof(Cell) * maze->m_width);
        for (int j = 0; j < maze->m_width; ++j) {
            count = fscanf(f, "%d", (int*) (&(maze->m_cells[i][j].m_walls)));
            if (count != 1) {
                printf ("ERROR: File %s has invalid format.\n", input);
                free(maze);
                fclose(f);
                return NULL;
            }
        }
    }
    if (!check_maze(maze)) {
        free(maze);
        fclose(f);
        return NULL;
    }
    fclose(f);
    return maze;
}

void print_horizontal_wall()
{
    putchar('-');
    putchar('-');
    putchar('-');
}

void print_horizontal_space()
{
    putchar(' ');
    putchar(' ');
    putchar(' ');
}

void display_maze(const Maze* maze)
{
    for (int i = 0; i < maze->m_height; ++i) {
        for (int j = 0; j < maze->m_width; ++j) {
            if (is_up_wall(&maze->m_cells[i][j])) {
                print_horizontal_wall();
            } else {
                print_horizontal_space();
            }
        }
        putchar('\n');
        for (int j = 0; j < maze->m_width; ++j) {
            if (is_left_wall(&maze->m_cells[i][j])) {
                putchar('|');
            } else {
                putchar(' ');
            }
            if (i == maze->m_start_y && j == maze->m_start_x) {
                putchar('s');
            } else if (i == maze->m_end_y && j == maze->m_end_x) {
                putchar('e');
            } else {
                putchar(' ');
            }
            putchar(' ');
        }
        if (is_right_wall(&maze->m_cells[i][maze->m_width - 1])) {
            putchar('|');
        } else {
            putchar(' ');
        }
        putchar('\n');
    }
    for (int j = 0; j < maze->m_width; ++j) {
        if (is_bottom_wall(&maze->m_cells[maze->m_height - 1][j])) {
            print_horizontal_wall();
        } else {
            print_horizontal_space();
        }
    }
    putchar('\n');
}

typedef struct vertex_t {
    int m_x;
    int m_y;
    int m_path_length;
    struct vertex_t* m_prev;
    int m_visited;
    struct vertex_t* m_neighbours[4];
} Vertex;

typedef struct {
    Vertex** m_vertices;
    int m_height;
    int m_width;
    int m_target_y;
    int m_target_x;
} Graph;

Graph* construct_graph(const Maze* maze)
{
    Graph* graph = (Graph*) malloc(sizeof(Graph));
    graph->m_vertices = (Vertex**) malloc(maze->m_height * sizeof(Vertex*));
    for (int i = 0; i < maze->m_height; ++i) {
        graph->m_vertices[i] = (Vertex*) malloc(maze->m_width * sizeof(Vertex));
    }
    for (int i = 0; i < maze->m_height; ++i) {
        for (int j = 0; j < maze->m_width; ++j) {
            graph->m_vertices[i][j].m_x = j;
            graph->m_vertices[i][j].m_y = i;
            graph->m_vertices[i][j].m_path_length = -1;
            graph->m_vertices[i][j].m_prev = NULL;
            graph->m_vertices[i][j].m_visited = 0;
            int x = 0;
            if (i != 0) {
                if (!is_up_wall(&(maze->m_cells[i][j]))) {
                    graph->m_vertices[i][j].m_neighbours[x] = &(graph->m_vertices[i - 1][j]);
                    ++x;
                }
            }
            if (j != 0) {
                if (!is_left_wall(&(maze->m_cells[i][j]))) {
                    graph->m_vertices[i][j].m_neighbours[x] = &(graph->m_vertices[i][j - 1]);
                    ++x;
                }
            }
            if (i != maze->m_height - 1) {
                if (!is_bottom_wall(&(maze->m_cells[i][j]))) {
                    graph->m_vertices[i][j].m_neighbours[x] = &(graph->m_vertices[i + 1][j]);
                    ++x;
                }
            }
            if (j != maze->m_width - 1) {
                if (!is_right_wall(&(maze->m_cells[i][j]))) {
                    graph->m_vertices[i][j].m_neighbours[x] = &(graph->m_vertices[i][j + 1]);
                    ++x;
                }
            }
            for (; x < 4; ++x) {
                graph->m_vertices[i][j].m_neighbours[x] = NULL;
            }
        }
    }
    graph->m_width = maze->m_width;
    graph->m_height = maze->m_height;
    graph->m_vertices[maze->m_start_y][maze->m_start_x].m_path_length = 0;
    graph->m_target_x = maze->m_end_x;
    graph->m_target_y = maze->m_end_y;
}

Vertex* get_minimal_unvisited_vertex(const Graph* graph)
{
    Vertex* res = NULL;
    for (int i = 0; i < graph->m_height; ++i) {
        for (int j = 0; j < graph->m_width; ++j) {
            Vertex* v = &(graph->m_vertices[i][j]);
            if (v->m_visited == 0 && v->m_path_length != -1) {
                if (res == NULL || res->m_path_length > v->m_path_length) {
                    res = v;
                }
            }
        }
    }
    return res;
}

char* dijkstra(Graph* graph)
{
    Vertex* cur = NULL;
    while (cur = get_minimal_unvisited_vertex(graph)) {
        cur->m_visited = 1;
        if (cur == &(graph->m_vertices[graph->m_target_y][graph->m_target_x])) {
            break;
        }
        for (int i = 0; i < 4; ++i) {
            if (cur->m_neighbours[i] == NULL) {
                break;
            }
            if (cur->m_neighbours[i]->m_path_length == -1 || cur->m_neighbours[i]->m_path_length > cur->m_path_length + 1) {
                cur->m_neighbours[i]->m_path_length = cur->m_path_length + 1;
                cur->m_neighbours[i]->m_prev = cur;
            }
        }
    }
    Vertex* target = &(graph->m_vertices[graph->m_target_y][graph->m_target_x]);
    if (target->m_visited == 0) {
        return NULL;
    }
    char* res = (char*) malloc(sizeof(char) * target->m_path_length + 1);
    res[target->m_path_length] = 0;
    while (target->m_prev != NULL) {
        Vertex* p = target->m_prev;
        int dx = p->m_x - target->m_x;
        int dy = p->m_y - target->m_y;
        target = p;
        if (dx == 1) {
            res[target->m_path_length] = 'L';
            continue;
        }
        if (dx == -1) {
            res[target->m_path_length] = 'R';
            continue;
        }
        if (dy == 1) {
            res[target->m_path_length] = 'U';
            continue;
        }
        if (dy == -1) {
            res[target->m_path_length] = 'D';
            continue;
        }
    }
    return res;
}

char* find_path(const Maze* maze)
{
    Graph* graph = construct_graph(maze);
    char* path = dijkstra(graph);
    return path;
}

void output_path(const char* output, const char* path)
{
    FILE* f = fopen(output, "w");
    if (f == NULL) {
        printf ("ERROR: Failed to open file %s for writing.\n", path);
        return;
    }
    fprintf(f, "%d\n", (int)strlen(path));
    fputs(path, f);
    fclose(f);
}
